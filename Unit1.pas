(*
  Gioco di dati elettronico creato da Aloe Luigi
  prendendo spunto da questo articolo in vendita
  https://www.amazon.it/dp/B07Z5BGL8D/ref=sspa_dk_detail_4?psc=1
  &spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzS0xZWU5aQzdaRVBSJmVuY3J5cHRlZElkPUEwOTg3MDU
  5M05OQldEV0I5UUYwQSZlbmNyeXB0ZWRBZElkPUEwMTUzODA4MTlaWlk3UURYTEtBSSZ3aWRnZXROY
  W1lPXNwX2RldGFpbDImYWN0aW9uPWNsaWNrUmVkaXJlY3QmZG9Ob3RMb2dDbGljaz10cnVl
*)
unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
const
  azioni: TArray<String> = ['Bacia', 'Tocca', 'Succhia', 'Soffia', 'Mordi'];
const
  parti: TArray<String> = ['Ombelico', 'Coscie', 'Labbra', 'Mano', 'Collo',
    'Orecchie','Sedere','A scelta...'];
const
dove:
TArray < String >= ['sul Tavolo', 'in Cucina', 'sul Divano', ' in Terrazzo'];
var
  _come, _dove, luogo: string;
  i: integer;
begin
  for i := 1 to 3 do
    with TPanel(FindComponent('Panel' + IntToStr(i))) do
    begin
      Font.Color := clwhite;
      Color := RGB(Random(256),Random(256),Random(256));
    end;
  Randomize;
  _come := azioni[Random(Length(azioni))];
  Randomize;
  Panel1.Caption := _come;
  _dove := parti[Random(Length(parti))];
  Randomize;
  Panel2.Caption := _dove;
  luogo := dove[Random(Length(dove))];
  Randomize;
  Panel3.Caption := luogo;
end;

end.
